﻿<?php

class Db {


    private static $connection;

		private static $settings = array(

		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,

		PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",

		PDO::ATTR_EMULATE_PREPARES => false,

	);



	

    public static function connection($server, $user, $password, $database) {

		if (!isset(self::$connection)) {

			self::$connection = @new PDO(

				"mysql:host=$server;dbname=$database",

				$user,

				$password,

				self::$settings

			);

		}

	}

	

	

    public static function queryOne($query, $parameters = array()) {

		$return = self::$connection->prepare($query);

		$return->execute($parameters);

		return $return->fetch();

	}



	

    public static function query($query, $parameters = array()) {

		$return = self::$connection->prepare($query);

		$return->execute($parameters);

		return $return->rowCount();

	}

	

	

    public static function getLastId()

	{

		return self::$connerction->lastInsertId();

	}

}